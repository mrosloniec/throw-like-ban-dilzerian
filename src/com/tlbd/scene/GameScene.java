package com.tlbd.scene;

import com.tlbd.manager.GameManager;
import com.tlbd.manager.ResourcesManager;
import com.tlbd.scene.listener.SceneTouchListener;
import com.tlbd.util.ColorRGB;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.*;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.ITextureRegion;
import static com.tlbd.util.Constants.*;

public class GameScene extends MainScene {

	private Rectangle pointer;
	private Rectangle bitch;
	private int countdown = 60;

	@Override
	public void createScene() {
		ResourcesManager.getInstance().loadBackground();
		ResourcesManager.getInstance().loadDan();

		setOnSceneTouchListener(new SceneTouchListener(this));

		addBackground();
		addTimer();
		addDan();
		addPointer();
		addBitch();
	}

	private void endGame() {
		GameManager.getInstance().setGameActive(false);
		pointer.setVisible(false);
	}

	private void addTimer() {
		final Text timer = new Text(0, 0, resourcesManager.font, "60", vertexBufferObjectManager);

		TimerHandler timerHandler = new TimerHandler(1, true, new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				if (countdown == 0) {
					endGame();
					return;
				}
				countdown--;
				timer.setText(String.valueOf(countdown));
			}
		});
		timer.registerUpdateHandler(timerHandler);
		attachChild(timer);
	}

	private void addPointer() {
		if (pointer == null) {
			pointer = new Rectangle(0, 0, 20, 20, vertexBufferObjectManager);
			pointer.setColor(new ColorRGB(0, 0, 0));
			pointer.setVisible(false);
			float[] xPath = {0, 80, 250, 425, 480, 450, 250, 80, 0};
			float[] yPath = {100, 20, 180, 350, 180, 20, 180, 350, 100};
			pointer.registerEntityModifier(new LoopEntityModifier(new PathModifier(2, new PathModifier.Path(xPath, yPath))));
			attachChild(pointer);
		}
	}

	public void addBitch() {
		if (bitch == null) {
			bitch = new Rectangle(300, 680, 50, 100, vertexBufferObjectManager);
			bitch.setColor(new ColorRGB(0, 0, 0));
			JumpModifier jumpModifier = new JumpModifier(1.2f, 600, 100, 600, 600, 50) {
				@Override
				protected void onModifierFinished(IEntity pItem) {
					pointer.setVisible(true);
				}
			};
			bitch.registerEntityModifier(jumpModifier);
			attachChild(bitch);
		}
	}

	private void addBackground() {
		ITextureRegion background = resourcesManager.getBackground();
		Sprite rectangle = new Sprite(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, background, vertexBufferObjectManager);
		rectangle.setZIndex(-1);
		attachChild(rectangle);
	}

	private void addDan() {
		ITextureRegion dan = resourcesManager.getDan();
		Sprite danSprite = new Sprite(10, 635, dan, vertexBufferObjectManager);
		attachChild(danSprite);
	}

	public Rectangle getPointer() {
		return pointer;
	}

	public Rectangle getBitch() {
		return bitch;
	}

	public void setBitch(Rectangle bitch) {
		this.bitch = bitch;
	}
}
