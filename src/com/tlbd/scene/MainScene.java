package com.tlbd.scene;

import com.tlbd.activity.MainActivity;
import com.tlbd.manager.ResourcesManager;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class MainScene extends Scene {

	protected Engine engine;
	protected MainActivity activity;
	protected ResourcesManager resourcesManager;
	protected VertexBufferObjectManager vertexBufferObjectManager;
	protected Camera camera;

	public MainScene() {
		this.resourcesManager = ResourcesManager.getInstance();
		this.engine = resourcesManager.engine;
		this.activity = resourcesManager.activity;
		this.vertexBufferObjectManager = resourcesManager.vertexBufferObjectManager;
		this.camera = resourcesManager.camera;
		createScene();
	}

	public abstract void createScene();
}