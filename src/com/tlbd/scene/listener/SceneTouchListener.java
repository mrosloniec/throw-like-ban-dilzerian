package com.tlbd.scene.listener;

import com.tlbd.manager.GameManager;
import com.tlbd.scene.GameScene;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

public class SceneTouchListener implements IOnSceneTouchListener {

	private GameScene gameScene;

	public SceneTouchListener(GameScene gameScene) {
		this.gameScene = gameScene;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if (!GameManager.getInstance().isGameActive()) {
			return false;
		}
		if (pSceneTouchEvent.isActionDown()) {
			final Rectangle pointer = gameScene.getPointer();
			float x = pointer.getX();
			float y = pointer.getY();
			pointer.setVisible(false);
			final Rectangle bitch = gameScene.getBitch();
			bitch.registerEntityModifier(new MoveModifier(0.7f, bitch.getX(), x, bitch.getY(), y) {
				@Override
				protected void onModifierFinished(IEntity pItem) {
					pointer.setVisible(true);
					bitch.setVisible(false);
					gameScene.setBitch(null);
					gameScene.addBitch();
				}
			});
		}

		return false;
	}

}
