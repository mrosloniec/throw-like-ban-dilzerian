package com.tlbd.util;

import org.andengine.util.color.Color;

public class ColorRGB extends Color {

	public ColorRGB(float pRed, float pGreen, float pBlue) {
		super(pRed/255f, pGreen/255f, pBlue/255f);
	}

}
