package com.tlbd.util;

public interface Constants {
	int CAMERA_WIDTH = 480;
	int CAMERA_HEIGHT = 800;
	int CENTER_X = CAMERA_WIDTH / 2;
	int CENTER_Y = CAMERA_HEIGHT / 2;
}
