package com.tlbd.manager;

import android.graphics.Typeface;
import com.tlbd.activity.MainActivity;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class ResourcesManager {

	private static final ResourcesManager INSTANCE = new ResourcesManager();

	private ITextureRegion background;
	private ITextureRegion dan;

	public Engine engine;
	public MainActivity activity;
	public Camera camera;
	public Font font;
	public VertexBufferObjectManager vertexBufferObjectManager;

	private ResourcesManager() {
	}

	public static void prepareManager(Engine engine, MainActivity activity, Camera camera, VertexBufferObjectManager vertexBufferObjectManager) {
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().camera = camera;
		getInstance().vertexBufferObjectManager = vertexBufferObjectManager;
		getInstance().font = FontFactory.create(activity.getFontManager(), activity.getTextureManager(),
				256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 24, Color.WHITE_ABGR_PACKED_INT);
		getInstance().font.load();
	}

	public static ResourcesManager getInstance() {
		return INSTANCE;
	}

	public void loadBackground() {
		BitmapTextureAtlas mCardDeckTexture = new BitmapTextureAtlas(activity.getTextureManager(), 480, 800);
		BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCardDeckTexture, activity, "bg.png", 0, 0);
		mCardDeckTexture.load();
		background = TextureRegionFactory.extractFromTexture(mCardDeckTexture);
	}

	public void loadDan() {
		BitmapTextureAtlas mCardDeckTexture = new BitmapTextureAtlas(activity.getTextureManager(), 90, 149);
		BitmapTextureAtlasTextureRegionFactory.createFromAsset(mCardDeckTexture, activity, "dan.png", 0, 0);
		mCardDeckTexture.load();
		dan = TextureRegionFactory.extractFromTexture(mCardDeckTexture);
	}

	public ITextureRegion getBackground() {
		return background;
	}

	public ITextureRegion getDan() {
		return dan;
	}
}
