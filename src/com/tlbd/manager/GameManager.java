package com.tlbd.manager;

public class GameManager {

	private boolean gameActive = true;

	private static final GameManager INSTANCE = new GameManager();

	private GameManager() {
	}

	public static GameManager getInstance() {
		return INSTANCE;
	}

	public boolean isGameActive() {
		return gameActive;
	}

	public void setGameActive(boolean gameActive) {
		this.gameActive = gameActive;
	}
}
