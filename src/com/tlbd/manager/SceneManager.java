package com.tlbd.manager;

import com.tlbd.scene.GameScene;
import com.tlbd.scene.MainScene;
import org.andengine.engine.Engine;

import static org.andengine.ui.IGameInterface.OnCreateSceneCallback;

public class SceneManager {

	private static final SceneManager INSTANCE = new SceneManager();

	private MainScene currentScene;
	private Engine engine = ResourcesManager.getInstance().engine;

	public void setScene(MainScene scene) {
		engine.setScene(scene);
		currentScene = scene;
	}

	public static SceneManager getInstance() {
		return INSTANCE;
	}

	public MainScene getCurrentScene() {
		return currentScene;
	}

	public void loadGameScene(OnCreateSceneCallback pOnCreateSceneCallback) {
		MainScene gameScene = new GameScene();
		setScene(gameScene);
		currentScene = gameScene;
		pOnCreateSceneCallback.onCreateSceneFinished(gameScene);
	}

}
