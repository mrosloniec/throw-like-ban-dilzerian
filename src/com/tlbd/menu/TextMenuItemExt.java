package com.tlbd.menu;

import org.andengine.entity.scene.menu.item.TextMenuItem;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.IFont;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class TextMenuItemExt extends TextMenuItem {

	private Runnable runnable;

	public TextMenuItemExt(int pID, IFont pFont, CharSequence pText, VertexBufferObjectManager pVertexBufferObjectManager, Runnable runnable) {
		super(pID, pFont, pText, pVertexBufferObjectManager);

		this.runnable = runnable;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if (pSceneTouchEvent.isActionUp()) {
			runnable.run();
		}
		return true;
	}
}